import React from 'react';
import { connect } from 'react-redux';
import { contactsFetched } from './store/actions/index';
import { ContactsList } from "./contacts-list";
import { getFilteredContacts } from './store/selectors/getFilteredContacts';


export class App extends React.Component {
  state = {
    search: ''
  }

  componentDidMount() {
    fetch('https://randomuser.me/api/?format=json&results=50')
      .then(res => res.json())
      .then(json => this.props.contactsFetched(json.results))
  }

  render() {
console.log(this.props)
    return (
      <div >
        {this.props.contacts.length === 0 && (
          <div>Trwa ladowanie</div>
        )}
      
        <ContactsList contacts={this.props.contacts} />
      </div >
    )
  }
}

const mapStateToProps = state => {
  return {
    contacts: getFilteredContacts(state.contacts, state.contactsSearch)
  }
};

const mapDispatchToProps = { contactsFetched };
export const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

