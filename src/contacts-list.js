import * as React from "react";
import { ContactItem } from "./contact-item";
import {
    Card, Table, TableBody,
    TableSortLabel,
    TableCell,
    TableRow,
    Checkbox,
    Tooltip,
    IconButton,
    Icon,
    Menu,
    MenuList,
    MenuItem,
    ListItemIcon,
    ListItemText
} from '@material-ui/core';

const rows = [
    {
        id: 'image',
        align: 'left',
        disablePadding: true,
        label: '',
        sort: false
    },
    {
        id: 'name',
        align: 'left',
        disablePadding: false,
        label: 'Name',
        sort: true
    },
    {
        id: 'categories',
        align: 'left',
        disablePadding: false,
        label: 'Category',
        sort: true
    },
    {
        id: 'priceTaxIncl',
        align: 'left',
        disablePadding: false,
        label: 'Price',
        sort: true
    },
    {
        id: 'quantity',
        align: 'left',
        disablePadding: false,
        label: 'Quantity',
        sort: true
    },
    {
        id: 'active',
        align: 'left',
        disablePadding: false,
        label: 'Active',
        sort: true
    }
];
export class ContactsList extends React.Component {

    contactToContactItem = contact => {
        const avatarUrl = contact.picture.thumbnail;
        const { title, first, last } = contact.name;
        const name = `${title} ${first} ${last}`.trim();
        const phone = contact.phone;
        const key = contact.login.username;
        console.log(this.props.contacts)
        return <ContactItem key={key} avatarUrl={avatarUrl} name={name} phone={phone} />;
    };
    state = {
        selectedProductsMenu: null
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    openSelectedProductsMenu = (event) => {
        this.setState({ selectedProductsMenu: event.currentTarget });
    };

    closeSelectedProductsMenu = () => {
        this.setState({ selectedProductsMenu: null });
    };


    render() {
        const { onSelectAllClick, order, orderBy, numSelected, rowCount, classes } = this.props;
        const { selectedProductsMenu } = this.state;

        return (
            <Card >
                <Table >

                    <TableBody >

                        <TableCell padding="checkbox" className="relative pl-4 sm:pl-12" >

                            <Checkbox
                                indeterminate={numSelected > 0 && numSelected < rowCount}
                                checked={numSelected === rowCount}
                                onChange={onSelectAllClick}
                            />
                            {numSelected > 0 && (
                                <div>
                                    <IconButton
                                        aria-owns={selectedProductsMenu ? 'selectedProductsMenu' : null}
                                        aria-haspopup="true"
                                        onClick={this.openSelectedProductsMenu}
                                    >
                                        <Icon>more_horiz</Icon>
                                    </IconButton>
                                    <Menu
                                        id="selectedProductsMenu"
                                        anchorEl={selectedProductsMenu}
                                        open={Boolean(selectedProductsMenu)}
                                        onClose={this.closeSelectedProductsMenu}
                                    >
                                        <MenuList>
                                            <MenuItem
                                                onClick={() => {
                                                    this.closeSelectedProductsMenu();
                                                }}
                                            >
                                                <ListItemIcon className={classes.icon}>
                                                    <Icon>delete</Icon>
                                                </ListItemIcon>
                                                <ListItemText inset primary="Remove" />
                                            </MenuItem>
                                        </MenuList>
                                    </Menu>
                                </div>
                            )}

                        </TableCell>
                        {rows.map(row => {
                            return (
                                <TableCell
                                    key={row.id}
                                    align={row.align}
                                    padding={row.disablePadding ? 'none' : 'default'}
                                    sortDirection={orderBy === row.id ? order : false}
                                >
                                    {row.sort && (
                                        <Tooltip
                                            title="Sort"
                                            placement={row.align === "right" ? 'bottom-end' : 'bottom-start'}
                                            enterDelay={300}
                                        >
                                            <TableSortLabel
                                                active={orderBy === row.id}
                                                direction={order}
                                                onClick={this.createSortHandler(row.id)}
                                            >
                                                {row.label}
                                            </TableSortLabel>
                                        </Tooltip>
                                    )}
                                </TableCell>
                            );
                        }, this)}
                        {this.props.contacts.map((item, index) => {
                            return (
                                <TableRow key={index}>
                                    <TableCell component="th" scope="row">
                                        {index}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        <img src={item.avatarUrl} alt="" />
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {item.name.first.charAt(0).toUpperCase() + item.name.first.slice(1)}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {item.name.last.charAt(0).toUpperCase() + item.name.last.slice(1)}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {item.gender.charAt(0).toUpperCase() + item.gender.slice(1)}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {item.gender.charAt(0).toUpperCase() + item.gender.slice(1)}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {item.gender.charAt(0).toUpperCase() + item.gender.slice(1)}
                                    </TableCell>

                                </TableRow>

                            )
                        })}
                    </TableBody>
                </Table>
            </Card>
        )
    }
}